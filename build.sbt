lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := """local-video-cache""",
    organization := "",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.13.5",
    libraryDependencies ++= Seq(
      guice,
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    ),
  )
