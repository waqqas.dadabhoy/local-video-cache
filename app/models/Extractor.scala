package models

import java.nio.file.{Files, Path}
import java.time.{Instant, ZoneId}
import scala.jdk.CollectionConverters._
import scala.jdk.StreamConverters._

trait Extractor {
  def name: String

  def getVideos(root: Path): Seq[Video]
}

object Extractor {
  val extsToIgnore = Set("txt", "m3u8", "sh")

  val youtubeExtractor = new Extractor {
    val uploaderPatternWithName = """([A-Za-z0-9_-]+) \((.*)\)""".r
    val uploaderPatternWithoutName = """([A-Za-z0-9_-]+)""".r
    val videoFilenamePattern = """(.+)-([A-Za-z0-9_-]{11})\.([A-Za-z0-9]+)""".r

    private def parseVideoPath(videoPath: Path, uploader: Uploader): Option[Video] = {
      val videoFile = videoPath.toFile
      val lastModified = Instant.ofEpochMilli(videoFile.lastModified()).atZone(ZoneId.systemDefault())
      videoPath.getFileName.toString match {
        case videoFilenamePattern(title, id, ext) => Some(Video(this, uploader, id, lastModified, title, videoPath))
        case _ => println(s"Error parsing file: ${uploader.id}/${videoPath.getFileName}"); None
      }
    }

    override def name: String = "youtube"

    override def getVideos(root: Path): Seq[Video] = {
      val extractorDir = root.resolve(name)
      val uploaders = Files.list(extractorDir).toScala(Iterator).filter(d => Files.isDirectory(d)).map { d =>
        d.getFileName.toString match {
          case uploaderPatternWithName(id, name) => Uploader(id, name, d)
          case uploaderPatternWithoutName(id) => Uploader(id, "", d)
        }
      }.toList
      val videos = uploaders.flatMap { uploader =>
        val files = Files.list(uploader.path).toScala(Iterator)
          .filter(f => Files.isRegularFile(f))
          .filterNot(f => extsToIgnore.exists(ext => f.getFileName.toString.endsWith(s".$ext")))
          .toList
        val parsed = files.flatMap { videoPath => parseVideoPath(videoPath, uploader) }
        parsed
      }
      videos
    }
  }

  val extractors: Seq[Extractor] = Seq(
    youtubeExtractor
  )

  val extractorsWithNames = extractors.map(e => (e.name, e)).toMap

  def fromName(name: String): Option[Extractor] = extractorsWithNames.get(name)
}
