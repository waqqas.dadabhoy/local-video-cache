package models

import java.nio.file.Path

case class Uploader(id: String, name: String, path: Path) {
  def formattedName = s"$id ($name)"
}
