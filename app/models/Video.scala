package models

import java.nio.file.Path
import java.time.ZonedDateTime

case class Video(extractor: Extractor, uploader: Uploader, id: String, modified: ZonedDateTime, title: String, path: Path)
