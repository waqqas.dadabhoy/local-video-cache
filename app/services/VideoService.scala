package services

import models.{Extractor, Uploader, Video}

import java.nio.file.Path

class VideoService(rootPath: Path) {

  def videos: Iterable[Video] = Extractor.extractors.flatMap(e => e.getVideos(rootPath))
  def uploaders: Iterable[Uploader] = videos.groupBy(_.uploader).keys

  def getByExtractorAndId(extractorName: String, videoId: String): Option[Video] = {
    videos.find(v => v.id == videoId && v.extractor.name == extractorName)
  }
}
