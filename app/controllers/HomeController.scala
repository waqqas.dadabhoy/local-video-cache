package controllers

import play.api.Configuration

import javax.inject._
import play.api.mvc._
import services.VideoService

import java.nio.file.Paths
import java.time.format.DateTimeFormatter
import scala.concurrent.ExecutionContext

class HomeController @Inject()(config: Configuration, cc: ControllerComponents)(implicit ec: ExecutionContext) extends AbstractController(cc) {
  val videoListDateDisplayFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
  val videoService = new VideoService(Paths.get(config.get[String]("videos.dir")))

  def index(extractorName: String, uploaderId: String) = Action { implicit request =>
    val filtered = videoService.videos.filter { v =>
      (if (extractorName.isBlank) true else v.extractor.name == extractorName) &&
      (if (uploaderId.isBlank) true else v.uploader.id == uploaderId)
    }
    Ok(views.html.index(filtered, videoListDateDisplayFormat))
  }

  def playVideo(extractor: String, videoId: String) = Action { implicit request =>
    videoService.getByExtractorAndId(extractor, videoId) match {
      case Some(video) => Ok.sendFile(video.path.toFile)
      case None => NotFound
    }
  }
}
